FROM node:11.7.0-alpine
RUN deluser --remove-home node

RUN apk --no-cache add --virtual  \
  .gyp g++ gcc libgcc libstdc++ linux-headers make python && \
  apk add git && \
  npm install --quiet node-gyp -g
