Requirements
=================

 - mongodb
 - nodejs v10

Install the development environment with docker
=================

1) Install docker and docker-compose https://docs.docker.com/compose/install/
2) Create the .env file into the server directory

        $ cd ~/ruta/flights_landing/server
        $ cp .env.conf .env

3) Execute the following commands to create containers and install dependencies:

        $ cd ~/ruta/flights_landing
        $ docker-compose build
        $ docker-compose up (agregar el parámetro -d para ejecutar en modo detach)


4) Import the DB in mongo:

        $ cd ~/ruta/flights_landing
        $ docker-compose exec flights-db mongorestore -d landing dumps/landing


Useful commands
=================

stop and shutdowns containers:

    $ docker-compose down

access into the container:

    $ docker-compose exec container_name /bin/sh

list containers:

    $ docker ps -a
    $ docker-compose ps

list images:

    $ docker images

delete image:

    $ docker rmi id-imagen