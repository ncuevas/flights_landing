import PackageStay from '../models/packageStay';
import Package from '../models/package';


export const resolvers = {
    Query: {
        async getAllPackageStays(_, { city }) {
            let result = city ? await PackageStay.find(
                {city:{$regex: new RegExp(city, "i")}}) : await PackageStay.find().limit(5);
            return result;
        },
        async getPackageStay(_, { _id }) {
            return await PackageStay.findById(_id);
        },
        async getAllPackages(_, { city }) {
            let result = city ? await Package.find(
                {city:{$regex: new RegExp(city, "i")}}) : await Package.find().limit(20);
            return result;
        },
        async getPackage(_, { _id }) {
            return await Package.findById(_id);
        }
    },
    Mutation: {
        async createPackageStay(_, { input }) {
            return await PackageStay.create(input);
        },
        async updatePackageStay(_, { _id, input }) {
            return await PackageStay.findOneAndUpdate({ _id }, input, { new: true })
        },
        async deletePackageStay(_, { _id }) {
            return await PackageStay.findByIdAndRemove(_id);
        },
        async createPackage(_, { input }) {
            return await Package.create(input);
        },
        async updatePackage(_, { _id, input }) {
            return await Package.findOneAndUpdate({ _id }, input, { new: true })
        },
        async deletePackage(_, { _id }) {
            return await Package.findByIdAndRemove(_id);
        }
    }
}
