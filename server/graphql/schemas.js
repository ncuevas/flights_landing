import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers';

const typeDefs = `

    type PackageStay {
        _id : ID!
        title: String!
        bestPrice: Float!
        city: String
    }

    type Package {
        _id : ID!
        parentId: ID
        scales: String!
        price: Float!
        description: String!
        link: String!
        img: String!
        city: String
    }

    type Query {
        getPackageStay(_id: ID!) : PackageStay
        getAllPackageStays(city: String): [PackageStay]

        getPackage(_id: ID!) : Package
        getAllPackages(city: String): [Package]
    }

    input PackageStayInput{
        title: String!
        bestPrice: Float!
        city: String!
    }

    input PackageInput {
        parentId: ID
        scales: String!
        price: Float!
        description: String!
        link: String!
        img: String!
        city: String!
    }

    type Mutation {
        createPackageStay(input: PackageStayInput) : PackageStay!
        updatePackageStay(_id: ID!, input: PackageStayInput) : PackageStay!
        deletePackageStay(_id: ID!) : PackageStay

        createPackage(input: PackageInput) : Package!
        updatePackage(_id: ID!, input: PackageInput) : Package!
        deletePackage(_id: ID!) : Package
    }
`;

export default makeExecutableSchema({
    typeDefs,
    resolvers
});