import  mongoose from 'mongoose';

import  PackageStay from './packageStay';

const Schema = mongoose.Schema;

const packageSchema = new Schema({
    parentId: {
        type: Schema.Types.ObjectId,
        ref: 'PackageStay'
    },
    scales : {
        type: String,
        required: true
    },
    price: {
        type: Number
    },
    description : {
        type: String,
        required: true
    },
    link : {
        type: String,
        required: true
    },
    img : {
        type: String,
        required: true
    },
    city : {
        type: String,
        required: true
    }
});

export default  mongoose.model('Package', packageSchema);
