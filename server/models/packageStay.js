import  mongoose from 'mongoose';

const Schema = mongoose.Schema;

const packageStaySchema = new Schema({
    title : {
        type: String,
        required: true
    },
    bestPrice: {
        type: Number
    },
    city : {
        type: String,
        required: true
    }
});

export default  mongoose.model('PackageStay', packageStaySchema);
