require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const cors = require('cors');
const mongoose = require('mongoose');

import schemas from './graphql/schemas';
import PackageStay from './models/packageStay';
import Package from './models/package';


mongoose.Promise = global.Promise;
mongoose.connect(
  'mongodb://'+ process.env.DB_HOST + '/' + process.env.DB_NAME,
  {useNewUrlParser: true});


const app = express();

app.use(cors());

app.use('/graphql', graphqlHTTP({
  schema: schemas,
  graphiql: true,
  context: {
    PackageStay,
    Package
  }
}));

app.listen(process.env.PORT);
