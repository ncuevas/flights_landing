export const schema = `
  type PackageStay {
        _id : ID!
        title: String!
        bestPrice: Float!
        city: String
    }

    type Package {
        _id : ID!
        parentId: ID
        scales: String!
        price: Float!
        description: String!
        link: String!
        img: String!
        city: String
    }

    type Query {
        getAllPackageStays(city: String): [PackageStay]
        getAllPackages(city: String): [Package]
    }

    input PackageStayInput{
        title: String!
        bestPrice: Float!
        city: String!
    }

    input PackageInput {
        parentId: ID
        scales: String!
        price: Float!
        description: String!
        link: String!
        img: String!
        city: String!
    }
`;

export const resolvers = {
  Query: {
    getAllPackages: (_, { city }) => ({
      name: city,
      url: `//localhost:9000/graphql`,
      repositories: {
        getAllPackages:[
          {
            "_id": "5c8ef3d53cd7e20aaf786424",
            "parentId": "5c8ec561b84a7f0675544480",
            "price": 26119,
            "description": "7 días playa",
            "city": "miami",
            "scales": "Directo",
            "link": "https://www.avantrip.com",
            "img": "https://static.avantrip.com/fkt-flight/images/cluster-new-york.jpg"
          },
          {
            "_id": "5c8ef3d53cd7e20aaf786425",
            "parentId": "5c8ec561b84a7f0675544480",
            "price": 30000,
            "description": "7 días playa básico",
            "city": "miami",
            "scales": "Directo",
            "link": "https://www.avantrip.com",
            "img": "https://static.avantrip.com/fkt-flight/images/cluster-new-york.jpg"
          },
          {
            "_id": "5c8ef3d53cd7e20aaf786426",
            "parentId": "5c8ec561b84a7f0675544480",
            "price": 40000,
            "description": "7 días playa media pensión",
            "city": "miami",
            "scales": "Directo",
            "link": "https://www.avantrip.com",
            "img": "https://static.avantrip.com/fkt-flight/images/cluster-new-york.jpg"
          }
        ],
      },
    }),
    getAllPackageStays: (_, { city }) => ({
      name: city,
      url: `//localhost:9000/graphql`,
      repositories: {
        getAllPackageStays: [
          {
            "_id": "5c8e96e2162db405b8d3dcac",
            "bestPrice": 26119,
            "title": "Todas las Estadias",
            "city": "miami"
          },
          {
            "_id": "5c8ec561b84a7f0675544480",
            "bestPrice": 26119,
            "title": "Viajá por 7 días",
            "city": "miami"
          },
          {
            "_id": "5c8ec572b84a7f0675544481",
            "bestPrice": 32413,
            "title": "Viajá por 10 días",
            "city": "miami"
          }
        ],
      },
    })
  }
};
