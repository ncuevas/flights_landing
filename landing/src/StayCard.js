import React, { Component } from 'react';
import styled from 'styled-components';
import {
    A,
    Card,
    CardBlock,
    CardTitle,
    CardLink,
    CardText,
} from '@bootstrap-styled/v4';

const StayCardWrap = styled(Card)`
    margin-top: 1rem;
    margin-left: 2rem;
    border-radius: 2px;
    border-bottom: 2px solid rgba(0, 0, 0, 0.32);
    width: 11rem;
    height: 3.6rem;
    transition: 0.3s ease;
    cursor: pointer;
    box-shadow: 0px 2px 2px rgba(0,0,0,0.12);
    border-radius: 2px;

    .card-block {
        justify-content: center;
        padding: 5px 10px;

        .card-text {
            font-size: 11px;
            font-weight: 300;
            color: #4a4a4a;
            margin-bottom: 5px;
        }

        .card-link {
            font-size: 14px;
            font-weight: 800;
            font-family: Roboto;
            color: #4a90e2;
        }
    }

    @media (max-width: 800px) {
        margin-top: 0rem;
        margin-left: 0rem;
        width: 20rem;
    }

`;

class StayCard extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
        <StayCardWrap className={`${this.props.selected && this.props.selected._id === this.props.data._id ? 'selected' : ''}`} onClick={() => {
                this.props.onClick(this.props.data);
            }
         }>
          <CardBlock>
            <CardText>{this.props.data.title}</CardText>
            <CardLink>desde ${new Intl.NumberFormat('es-AR',{maximumSignificantDigits: 2}).format(this.props.data.bestPrice)}</CardLink>
          </CardBlock>
        </StayCardWrap>
    );
  }
}

export default StayCard;
