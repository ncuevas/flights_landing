import React, { Component } from 'react';
import gql from "graphql-tag";

import styled from 'styled-components';
import BootstrapProvider from '@bootstrap-styled/provider';
import {
    A,
    Card,
    CardBlock,
    CardLink,
    CardTitle,
    CardSubtitle,
    CardText,
    Container,
    Col,
    Hr,
    H1,
    Img,
    Jumbotron,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    P,
    Row,
    Strong
} from '@bootstrap-styled/v4';

import Logo from './assets/logo.png';
import LogoWhite from './assets/logowhite.png';
import SkyBg from './assets/sky.jpg';


import StayCard from './StayCard';
import PackageCard from './PackageCard';

import client from './apolloClient';

const assets = {
    'logo': Logo,
    'logoWhite': LogoWhite
};

const theme = {
  '$font-family-base': 'Roboto',
  '$enable-rounded': false,
};

const NavbarAv = styled(Navbar)`
  a.navbar-brand {
    color: #e1020b;
    font-size: 0.8rem;
    font-weight: 500;
    img {
        margin-right:1rem;
    }

  }
  .container {
    padding-right: 0;
    padding-left: 0;
  }
  @media (max-width: 800px) {
    background-color: #e1020b;
  }
`;

const LogoImg = styled(Img)`
    width: 3rem;
    @media (max-width: 800px) {
        display: none;
    }
`;

const LogoMobileImg = styled(Img)`
    width: 2.5rem;
    @media (min-width: 800px) {
        display: none;
    }
`;

const LandingCardTitle = styled(P)`
    margin-top: -2rem;
    font-size: 1.4rem;
    text-align: center;
    @media (max-width: 800px) {
        text-align: left;
        margin-top: 1rem;
        margin-left: 1rem
        margin-bottom: 3rem;
    }
`;

const JumbotronLanding = styled(Jumbotron)`
    background: url('${props => props.img}') no-repeat;
    color: #ffffffcf;
`;

const StayCardContainer = styled(Container)`
    margin-top: -6rem;
    .card.selected {
        border: 1px solid #32b2e8;
        background-color: #f2f9fd;
    }
    .card:hover{
        background-color: #f2f9fd;
    }
    .row{
        justify-content: center;
    }


    .card.hide {
        display: none;
    }

    @media (max-width: 1200px) and (min-width: 768px)  {
        .row{
            justify-content: end;
        }
    }

    @media (max-width: 800px) {
        padding-right: 0px;
        padding-left: 0px;

        .card.hide {
           display: flex;
        }

        .row{
            margin-right: 0px;
            margin-left: 0px;
        }

        .row.stay-list {
            display: none;
        }

        .row.stay-list.show {
            display: flex;
            -webkit-transition: opacity 600ms, visibility 600ms;
            transition: opacity 600ms, visibility 600ms;
        }
    }
`;

const PackageCardContainer = styled(Container)`
    .row {
        justify-content: center;
    }
    .hide {
        display:none;
        transition: opacity 1s ease-out;
    }
    @media (max-width: 1200px) and (min-width: 768px)  {
        .row{
            justify-content: end;
        }
    }
    @media (max-width: 800px) {
        padding-right: 0px;
        padding-left: 0px;
        .row{
            justify-content: center;
            margin-right: 0px;
            margin-left: 0px;
        }
    }
`;

const H1Container = styled(Container)`
    margin-top: 2rem;
    margin-bottom: 1rem;

    strong {
        text-transform: capitalize;
    }

    .row {
        justify-content: center;
    }
    h1 {
        font-weight: 100;
        color: #737976;
    }
    strong {
        font-weight: 900;
        color: #49474a;
    }

    @media (max-width: 800px) {
        padding-right: 0px;
        padding-left: 0px;
        h1 {
            font-size: 1.4rem;
        }
        .row{
            text-align: center;
            justify-content: center;
            margin-right: 0px;
            margin-left: 0px;
        }
    }
`;

const StayCardMobile = styled(Card)`
    border-radius: 2px;
    height: 3.6rem;
    box-shadow: 0px 2px 2px rgba(0,0,0,0.12);
    border-radius: 2px;

    .card-block {
        justify-content: center;
        padding: 5px 10px;

        .card-text {
            font-size: 14px;
            color: #4a4a4a;
            margin-bottom: 5px;
        }

        .card-link {
            font-size: 14px;
            font-weight: 800;
            font-family: Roboto;
            color: #4a90e2;
        }
    }

    .content-left {
        margin-top: 1rem;
        max-width: 65%;
        float: left;
    }

    .content-right {
        margin-top: 1rem;
        max-width: 100%;
        float: right;
    }

    @media (max-width: 800px) {
        margin-top: 0rem;
        margin-left: 0rem;
        width: 20rem;
    }

`;

class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      selectedStay: null,
      showAll: false,
      city: null,
      showList: false,
      stays: [],
      packages: []
    };
  }

  componentDidMount() {
    client
      .query({
        query: gql`{
          getAllPackages(city: ""){
            _id
            parentId
            price
            description
            city
            scales
            link
            img
          }

          getAllPackageStays(city: ""){
            _id
            bestPrice
            title
            city
          }
      }
        `
      })
      .then(result => {

            this.setState({
                city: result.data.getAllPackageStays.length ? result.data.getAllPackageStays[0].city : null,
                stays: result.data.getAllPackageStays,
                packages: result.data.getAllPackages
            });
      });
  }


  selectStay(data) {
    let hasChilds = this.state.packages.filter((elem) => elem.parentId === data._id).length ? true : false;
    this.setState({selectedStay:data, showAll:!hasChilds, showList:false});
  }

  capitalizeWord(word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
  }


  edit() {
    let showList = this.state.showList ? false : true;
    this.setState({showList: showList});
  }



  getTitle() {
    let title =
    <Strong>
        {!this.state.selectedStay
            ? ''
            : this.state.showAll ? this.state.selectedStay.title : ' ' + this.state.selectedStay.title + ' ' + this.state.city}
    </Strong>;

    return title;
  }

  render() {
    return (
    <BootstrapProvider theme={theme} reset>

        <NavbarAv>
            <Container>
            <div className="d-flex justify-content-between">
              <NavbarBrand tag={A} href="https://www.avantrip.com">
                <LogoImg alt="" src={assets.logo} />
                <LogoMobileImg alt="" src={assets.logoWhite} />
                 Viajar es la guita mejor invertida
              </NavbarBrand>
              <NavbarToggler />
            </div>

            </Container>
        </NavbarAv>

        <JumbotronLanding fluid img={SkyBg}>
            <LandingCardTitle>Los vuelos m&aacute;s baratos para tu estad&iacute;a

              {this.state.city
                ? ' en '+ this.capitalizeWord(this.state.city)
                : ''
              }

            </LandingCardTitle>
        </JumbotronLanding>


        <StayCardContainer>
            <Row>
            <StayCardMobile className="hide" data={this.state.selectedStay}>
              <CardBlock>
                <CardText className="content-left">
                <Strong>
                {
                    this.state.selectedStay
                    ? this.state.selectedStay.title + " " + this.capitalizeWord(this.state.city)
                    : 'Todas Las Estadías'
                }
                </Strong>
                </CardText>
                <CardLink className="content-right" onClick={this.edit.bind(this)}>
                {
                    this.state.showList
                    ? 'OCULTAR'
                    : 'EDITAR'
                }
                </CardLink>
              </CardBlock>
            </StayCardMobile>
            </Row>

            <Row className={"stay-list " + `${this.state.showList ? 'show' : ''}` }>
            {this.state.stays.map(
                (item,index) =>
                  <StayCard key={item._id} selected={this.state.selectedStay} data={item} onClick={this.selectStay.bind(this)}/>
                )
            }
            </Row>
        </StayCardContainer>

        <H1Container>
            <Row>
                <H1>Vuelos destacados
                {this.state.showAll
                    ? ' en '
                    : ''}
                {this.getTitle()}</H1>
            </Row>
        </H1Container>

        <PackageCardContainer >
            <Row>
                {this.state.packages.map(
                    (item,index) =>
                      <PackageCard key={item._id} data={item} selectedParent={this.state.selectedStay} show={this.state.showAll}/>
                    )
                }
            </Row>
        </PackageCardContainer>

    </BootstrapProvider>
    );
  }
}

export default App;
