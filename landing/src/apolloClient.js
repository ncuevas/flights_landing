import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const cache = new InMemoryCache();

const BASE_URL = '//localhost:9000/graphql';

const httpLink = new HttpLink({
  uri: BASE_URL
});

export default new ApolloClient({
  link: httpLink,
  cache,
});
