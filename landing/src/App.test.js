import React from 'react';
import ReactDOM from 'react-dom';

import 'cross-fetch/polyfill';


import App from './App';


it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
