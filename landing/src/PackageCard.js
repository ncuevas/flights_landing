import React, { Component } from 'react';
import styled from 'styled-components';
import {
    Card,
    CardImg,
    CardBlock,
    CardTitle,
    CardSubtitle,
    CardText,
    Button,
} from '@bootstrap-styled/v4';


const PackageCardWrap = styled(Card)`
    width: 22%;
    margin-left: 1rem;
    margin-top: 1rem;

    .card-block {
        padding: 0rem;
    }

    div.description {
        background-color: #fff;
        float: left;
        padding: 6px 6px 6px 6px;
        width: 100%;

        .content-left {
            max-width: 65%;
            float: left;
        }

        .content-right {
            max-width: 100%;
            float: right;
        }

        .currency {
            color: #e90010;
            font-size: 21px;
            font-weight: 900;
        }

        .amount {
            color: #e90010;
            font-size: 20px;
            font-weight: 900;
        }

        .price {
            display: block;
            font-size: 12px;
            margin-top: 5px;
        }

        .days {
            display: block;
            color: #e90010;
            font-size: 20px;
            font-weight: 900;
        }

        .flight {
            a {
                color: #5cace1;
                display: block;
                font-size: 14px;
                margin-top: 5px;
                margin-left: 6px;
                text-decoration: none;
            }
        }
    }

    @media (max-width: 800px) {
        width: 80%;
    }
`;

class PackageCard extends Component {


  render() {
    return (
        <PackageCardWrap className={`${this.props.selectedParent ? this.props.selectedParent._id !== this.props.data.parentId && !this.props.show ? 'hide' : '' : ''}`}>
          <CardImg top src={this.props.data.img} alt="Card image cap" />
          <CardBlock>
            <div className="description">

                <div className="content-left">
                  <span className="price">
                    {this.props.data.scales}
                  </span>
                  <span className="days">
                    {this.props.data.description}
                  </span>
                  <span className="days">
                  </span>
                </div>

                <div className="content-right">
                  <span className="price">
                    Precio desde
                  </span>
                  <span className="currency">
                    $
                  </span>
                  <span className="amount">
                    {new Intl.NumberFormat('es-AR',{maximumSignificantDigits: 2}).format(this.props.data.price)}
                  </span>
                  <span className="flight">
                    <a href={this.props.data.link} target="_blank">ver vuelo</a>
                  </span>
                </div>

            </div>
          </CardBlock>
        </PackageCardWrap>
    );
  }
}

export default PackageCard;
